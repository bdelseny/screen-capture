let action;
let thisBrowser;
/** Check if browser variable is defined, else we are on chrome environment */
if (typeof browser === "undefined") {
  thisBrowser = chrome;
  action = "action";
} else {
  action = "browserAction";
  thisBrowser = browser;
}

/**
 * Open the screen recorder page in a new tab
 */
thisBrowser[action].onClicked.addListener(() => {
  const createData = {
    url: "panel/panel.html",
  };
  thisBrowser.tabs.create(createData);
});
