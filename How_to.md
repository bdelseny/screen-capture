# How to

## JS Documentation

### Lint documentation

```bash
deno task doc-lint ./*.js ./**/*.js
```

### Export to MD

```bash
sh ./builder/build_doc.sh
```
