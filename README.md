## Screen capture

## Description

A firefox add-ons to do video screen capture.

It adds a button on your Firefox modules toolbar that opens a new tab on which
you can start recording your screens and windows.

## Badges

[![Pipeline](https://gitlab.com/bdelseny/screen-capture/badges/main/pipeline.svg)](https://gitlab.com/bdelseny/screen-capture/)
[![Coverage](https://gitlab.com/bdelseny/screen-capture/badges/main/coverage.svg)](https://gitlab.com/bdelseny/screen-capture/)

## Visuals

![screen capture](./assets/capture.webm)

## Installation

- Install the firefox extension:
  https://addons.mozilla.org/en-US/firefox/addon/screen-capture/

Or

- Go to the plain html page: https://bdelseny.gitlab.io/screen-capture

## Usage

- Click on the extension button or go to
  https://bdelseny.gitlab.io/screen-capture
- It opens a page on which you can `start capture`
- It opens a popup to let you choose what you want to capture
  - select one of the options - a screen or a window
- It stats recording what you are doing on the selected screen/window
  - It displays what you are doing on the extension page
- Once you have finish recording, click on `stop capture`
- The last screen capture image is shown
- Click on `download capture` to save it on your computer

## Support

You can report any bug or ask for new features in the issues.

## Roadmap

No Roadmap for now. I work on it when I have the time and the will.

## Contributing

- Create an issue

Or

- Prepare a merge request

WIP

## Authors and acknowledgment

- **Initiator**: Bastien Delseny @bdelseny

## License

GNU GENERAL PUBLIC LICENSE - Version 3, 29 June 2007

## Project status

Project updates will be slow as I work on it on my free time. You are free to
propose new features and fix through merge requests, you can also fork this
project to do your own add-on.
