/**
 * Get the current date at an exploitable format
 * @returns the current date as a simple format
 */
export function getCurrentDate() {
  const date = new Date();
  return `${
    (date.getMonth() + 1).toLocaleString([], { minimumIntegerDigits: 2 })
  }-${date.getDate().toLocaleString([], { minimumIntegerDigits: 2 })}-${
    date.getHours().toLocaleString([], { minimumIntegerDigits: 2 })
  }_${date.getMinutes().toLocaleString([], { minimumIntegerDigits: 2 })}_${
    date.getSeconds().toLocaleString([], { minimumIntegerDigits: 2 })
  }`;
}
