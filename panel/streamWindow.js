import { Stream } from "./stream.js";

const stream = new Stream({
  video: document.querySelector("video"),
  log: document.getElementById("log"),
  start: document.getElementById("start"),
  stop: document.getElementById("stop"),
  download: document.getElementById("download"),
});

const videoElem = document.querySelector("video");
const logElem = document.getElementById("log");
const startElem = document.getElementById("start");
const stopElem = document.getElementById("stop");
const downloadElem = document.getElementById("download");

let mediaRecorder = undefined;

// Set event listeners for the start and stop buttons
startElem.addEventListener("click", (_evt) => {
  startCapture();
}, false);

stopElem.addEventListener("click", (_evt) => {
  stopCapture();
  downloadElem.disabled = false;
}, false);

downloadElem.addEventListener("click", (_evt) => {
  stream.download();
}, false);

// Log averything usefull on the page instead of the console
console.log = (msg) => setLog(msg, "black");
console.error = (msg) => setLog(msg, "red");
console.warn = (msg) => setLog(msg, "orange");
console.info = (msg) => setLog(msg, "blue");

/**
 * Right stuff on the page log panel
 * @param {string} msg message to display
 * @param {string} color message color
 */
function setLog(msg, color) {
  const span = document.createElement("span");
  const br = document.createElement("br");
  span.style.color = color;
  span.innerText = msg;
  logElem.append(span, br);
}

/**
 * Start screen record and show the record on the video element
 */
async function startCapture() {
  videoElem.srcObject = null;
  logElem.innerHTML = "";

  try {
    const mediaStream = await navigator.mediaDevices.getDisplayMedia(
      stream.options.displayMediaStreamOptions,
    );
    mediaRecorder = new MediaRecorder(mediaStream, {
      mimeType: stream.options.mimeType,
    });
    mediaRecorder.ondataavailable = (event) =>
      stream.handleDataAvailable(event);
    mediaRecorder.start();
    videoElem.srcObject = mediaStream;
    dumpOptionsInfo();
  } catch (err) {
    console.error(`Error: ${err}`);
  }
}

/**
 * Stop screen record
 * @param {Event} _evt
 */
function stopCapture(_evt) {
  mediaRecorder.stop();
  const tracks = videoElem.srcObject.getTracks();

  tracks.forEach((track) => track.stop());
}

/**
 * Log recorder options info
 */
function dumpOptionsInfo() {
  /** @type {MediaStreamTrack} */
  const videoTrack = videoElem.srcObject.getVideoTracks()[0];

  console.info("Track settings:");
  console.info(JSON.stringify(videoTrack.getSettings(), null, 2));
  console.info("Track constraints:");
  console.info(JSON.stringify(videoTrack.getConstraints(), null, 2));
}
