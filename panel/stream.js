import { getCurrentDate } from "./dateFormatter.js";

export class Stream {
  /**
   * Manage Streaming
   * @param {HTMLVideoElement} elements.video video element
   * @param {HTMLDivElement} elements.log log element
   * @param {HTMLButtonElement} elements.start start button
   * @param {HTMLButtonElement} elements.stop stop button
   * @param {HTMLButtonElement} elements.download download button
   * @param {Object} options.displayMediaStreamOptions https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getDisplayMedia#parameters
   * @param {string} options.mimeType
   */
  constructor(elements = {
    video: new HTMLVideoElement(),
    log: new HTMLDivElement(),
    start: new HTMLButtonElement(),
    stop: new HTMLButtonElement(),
    download: new HTMLButtonElement(),
  }, options = {
    mimeType: MimeTypes.webm,
    displayMediaStreamOptions: {
      video: {
        displaySurface: "window",
      },
      audio: false,
    },
  }) {
    this.elements = elements;
    this.options = options;
    this.recordedChunks = [];
  }

  /**
   * Download the recorder screen capture
   */
  download() {
    const blob = new Blob(this.recordedChunks, {
      type: "video/webm",
    });
    const url = URL.createObjectURL(blob);
    const a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";
    a.href = url;
    a.download = `download_${getCurrentDate()}.webm`;
    a.click();
    window.URL.revokeObjectURL(url);
  }

  /**
   * Add data chunks to record if there are data
   * @param {Event} event
   */
  handleDataAvailable(event) {
    console.log("data-available");
    if (event.data.size > 0) {
      this.recordedChunks.push(event.data);
      console.log(JSON.stringify(this.recordedChunks));
    } else {
      // …
    }
  }
}

const MimeTypes = Object.freeze({
  webm: "video/webm",
  avi: "video/x-msvideo",
  mp4: "video/mp4",
  mpeg: "video/mpeg",
  ogv: "video/ogg",
  ts: "video/mp2t",
  gp: "video/3gpp",
  g2: "video/3gpp2",
});
