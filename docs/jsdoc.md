# JS Documentation

Table of Content

- [background-script.md](https://gitlab.com/bdelseny/screen-capture/-/blob/main/docs/background-script.md)
  - Source:
    [background-script.js](https://gitlab.com/bdelseny/screen-capture/-/blob/main/background-script.js)
- [panel/streamWindow.md](https://gitlab.com/bdelseny/screen-capture/-/blob/main/docs/panel/streamWindow.md)
  - Source:
    [panel/streamWindow.js](https://gitlab.com/bdelseny/screen-capture/-/blob/main/panel/streamWindow.js)
