#!/bin/bash

action=$1
[[ -n $action ]] || action='doc-md'

shopt -s globstar
mkdir docs
# Init js doc TOC
touch docs/jsdoc.md
cat >docs/jsdoc.md <<EOL
# JS Documentation

Table of Content

EOL
for f in **/*.js; do
  [[ "$f" == */* ]] && mkdir -p "docs/${f%/*}"
  echo "">docs/${f%.js}.md
  deno task $action "$f" -o "docs/${f%.js}.md"
  # Add header
  header=$(
    echo "# $f
"
    cat docs/${f%.js}.md
  )
  echo "$header" >docs/${f%.js}.md
  # Write TOC
  cat >>docs/jsdoc.md <<EOL
- [${f%.js}.md](https://gitlab.com/bdelseny/screen-capture/-/blob/main/docs/${f%.js}.md)
  - Source: [$f](https://gitlab.com/bdelseny/screen-capture/-/blob/main/$f)
EOL
done
